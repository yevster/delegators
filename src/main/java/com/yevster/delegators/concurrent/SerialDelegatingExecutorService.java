package com.yevster.delegators.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

/**
 * An executor service that delegates all submitted tasks to another executor
 * service with the following restrictions: All tasks submitted to this service
 * run serially, meaning one at a time and in the order of submission.
 *
 * This executor service is greedy and unfair. Any tasks submitted to this
 * executor service while any of its prior tasks are executed will be executed
 * immediately following, before returning the thread to the delegated executor
 * service.
 */
public class SerialDelegatingExecutorService implements ExecutorService {

	private final ExecutorService targetExecutorService;
	private final ConcurrentLinkedQueue<FutureTask<?>> queue = new ConcurrentLinkedQueue<>();
	private volatile boolean draining = false;
	private Lock drainStatusLock = new ReentrantLock();
	private Consumer<Throwable> errorHandler = null;

	public SerialDelegatingExecutorService(ExecutorService targetExecutorService) {
		this.targetExecutorService = targetExecutorService;
	}

	/**
	 * Sets a handler to process exceptions generated while running any of the
	 * tasks (e.g. logging). Note: Any exceptions thrown within the handler
	 * itself are discarded.
	 * 
	 * @param errorHandler
	 */
	public void setErrorHandler(Consumer<Throwable> errorHandler) {
		this.errorHandler = errorHandler;
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		FutureTask<T> retVal = new FutureTask<>(task);

		queue.offer(retVal);
		drainStatusLock.lock();
		try {
			if (!draining) {
				targetExecutorService.submit(this::drainQueue);
			}
		} finally {
			drainStatusLock.unlock();
		}
		return retVal;
	}

	private void drainQueue() {
		// Drain without locking first, to minimize contention.
		queue.stream().sequential().forEach(this::runSafely);

		drainStatusLock.lock();
		try {
			// Drain anything else that might have been added during critical
			// section
			queue.stream().sequential().forEach(this::runSafely);
		} finally {
			drainStatusLock.unlock();
		}
	}

	private void runSafely(FutureTask<?> r) {
		try {
			r.run();
		} catch (Throwable t) {
			if (errorHandler != null) {
				try {
					errorHandler.accept(t);
				} catch (Throwable t2) {
				}
			}
		}
	}
	
	@Override
	public void execute(Runnable command) {
		submit(command);
	}
	
	

}
