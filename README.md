[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# README #

This is a work-in-progress class library. Its members delegate to other, better known class libraries, while adding specific limitations.